import React from 'react';
import './TextBox.css';

const TextBox = (props) => {
    return(
        <div className="TextBox">
            <p>{props.text}</p>
        </div>
    );
};

export default TextBox;