import React from "react";

const Image = (props) => {
    return (
        <div className={"Image"}>
            <img src={props.imageSource} alt={"Scary PHP"}></img>
        </div>
    );
};

export default Image;