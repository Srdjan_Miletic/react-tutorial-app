import React from "react";
import TextBox from "./TextBox";
import Button from "./Button";
import Image from "./Image"

class Homepage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showSecret: false
        }
    }

    buttonClicked = () => {
        this.setState({ showSecret: !(this.state.showSecret) });
    };

    render() {
        console.log(this.state.showSecret);
        if (!this.state.showSecret) {
            return (
                <div className='Homepage'>
                    <TextBox text={"What are programmers scared of?"}/>
                    <Button text={"Are you brave enough to click me?"} onClick={this.buttonClicked}/>
                </div>
            );
        }
        else {
            return(
                <div className={"Homepage"}>
                    <Image imageSource={"../images/php_makes_me_sad.jpeg"}/>
                    <Button text={"RUN AWAY!"} onClick={this.buttonClicked}/>
                </div>
            );
        }
    }
}

export default Homepage;