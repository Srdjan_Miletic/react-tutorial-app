import React, {Component} from 'react';
import './App.css';
import Homepage from "./Components/Homepage";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonClicked: 0
        }
    }

    render() {
        return (
            <div className="App">
                <Homepage/>
            </div>
        );
    }
}

export default App;
